# RCloud

[This project](https://gitlab.com/b-data/docker/deployments/rcloud) serves as a
template to run [RCloud](https://gitlab.b-data.ch/rcloud/rcloud/container_registry) 
in a docker container using docker-compose.

**Features**

*  RCloud using _gitgist_ - local git repositories - for notebook storage
   (default).
    *  optional: Using [_GitHub_ gists](https://gist.github.com/) for notebook
       storage and _GitHub_ accounts for user authentication.
    *  Other functionality:
        *  Redis as the back-end for key/value RCloud Storage (RCS).
        *  SOLR to index gists and provide full-text search functionality.
        *  optional: SessionKeyServer for enhanced security (multi-user enviornment).
*  Pre-configured to run at a subdomain (rcloud) of your **own domain**:
    *  Automatic redirect from location `/` to `/login.R`.
*  Use of an .env file for variable substitution in the Compose file.

**About RCloud**

*  Homepage: https://rcloud.social/index.html
*  Documentation: https://rcloud.social/documentation/

## Prerequisites

The following is required:

*  A [Docker Deployment](https://gitlab.com/b-data/docker/deployments) of
   [Træfik](https://gitlab.com/b-data/docker/deployments/traefik).
*  A DNS record for subdomain **rcloud** pointing to this host.

## Setup

1.  Create an external docker network named "rcloud":  
    ```bash
    docker network create rcloud
    ```
1.  Make a copy of '[.env.sample](.env.sample)' and rename it to '.env'.
1.  Update environment variables `RC_DOMAIN` and
    `RC_CERTRESOLVER_NAME` in '.env':
    *  Replace `mydomain.com` with your **own domain** that serves the
       subdomains.
    *  Replace `mydomain-com` with a valid certificate resolvers name of Træfik.
1.  Make a copy of '[rcloud.conf.sample](rcloud.conf.sample)' and
    rename it to 'rcloud.conf'.
    *  optional: [Register RCloud as an OAuth App](https://developer.github.com/apps/building-oauth-apps/creating-an-oauth-app/) on GitHub:  
       ```
       Application name: RCloud
       Homepage URL: https://rcloud.mydomain.com
       Authorization callback URL: https://rcloud.mydomain.com/login_successful.R
       ```
        *  Uncomment lines 8-12 in `rcloud.conf` and replace
           `github.client.id` and `github.client.secret`.
        *  Comment lines 13-15 to disable _gitgist_ for local notebook storage.
        *  Uncomment line 23 to enable the SessionKeyServer.
1.  Make a copy of '[docker-compose.yml.sample](docker-compose.yml.sample)' and
    rename it to 'docker-compose.yml'.
    *  optional: Uncomment lines 26 and 47-52 to enable the SessionKeyServer.
1.  Precreate folders `data` and `solr/data`:  
    ```bash
    mkdir data
    sudo chown 1000:1000 data
    mkdir -p solr/data
    sudo chown 8983:8983 solr/data
    ```
1.  Start the container in detached mode:  
    ```bash
    docker-compose up -d
    ```

### Test

Wait a bit and visit https://rcloud.mydomain.com to confirm everything went
fine:

*  With _gitgist_ enabled you will be logged in as user `rcloud` automatically.
*  With _GitHub_ enabled you will be forwarded to the GitHub login page.

## Further reading

*  [Tutorials](https://rcloud.social/tutorials/index.html)
*  [Setting up RCloud](https://github.com/att/rcloud/blob/develop/doc/INSTALL.md)
