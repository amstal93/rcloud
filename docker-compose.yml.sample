version: '3'

services:
  rcloud:
    image: registry.gitlab.b-data.ch/rcloud/rcloud
    container_name: rcloud
    restart: always
    networks:
      - default
      - webproxy
    volumes:
      - ./data:/data/rcloud/data
      - ./rcloud.conf:/data/rcloud/conf/rcloud.conf
    labels:
      - traefik.enable=true
      - traefik.docker.network=webproxy
      - traefik.http.routers.rcloud.entrypoints=web
      - traefik.http.middlewares.rcloud-redirect.redirectregex.regex=^https?://rcloud.${RC_DOMAIN}/$$
      - traefik.http.middlewares.rcloud-redirect.redirectregex.replacement=https://rcloud.${RC_DOMAIN}/login.R
      - traefik.http.middlewares.rcloud-redirect.redirectregex.permanent=true
      - traefik.http.routers.rcloud.rule=Host(`rcloud.${RC_DOMAIN}`)
      - traefik.http.routers.rcloud.middlewares=rcloud-redirect,http2https@file
      - traefik.http.routers.rcloud-sec.entrypoints=websecure
      - traefik.http.routers.rcloud-sec.rule=Host(`rcloud.${RC_DOMAIN}`)
      - traefik.http.routers.rcloud-sec.tls.certresolver=${RC_CERTRESOLVER_NAME}
      - traefik.http.routers.rcloud-sec.middlewares=rcloud-redirect,sts@file
    depends_on:
      - rcloud-redis
      - rcloud-solr
      #- rcloud-sks

  rcloud-redis:
    image: redis:5
    container_name: rcloud-redis
    restart: always
    volumes:
      - ./redis/data:/data

  rcloud-solr:
    image: registry.gitlab.b-data.ch/rcloud/rcloud-solr
    container_name: rcloud-solr
    restart: always
    volumes:
      - ./solr/data:/var/solr
    entrypoint:
      - docker-entrypoint.sh
      - solr-precreate
      - rcloudnotebooks
      - /opt/solr/server/solr/configsets/rcloudnotebooks

#  rcloud-sks:
#    image: registry.gitlab.b-data.ch/rcloud/rcloud-sks
#    container_name: rcloud-sks
#    restart: always
#    volumes:
#      - ./sks/data:/usr/src/sks/data

networks:
  default:
    external:
      name: rcloud
  webproxy:
    external: true
